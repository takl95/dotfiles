set nocompatible
filetype plugin on
syntax on
call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree'
Plug 'vim-scripts/argtextobj.vim'
Plug 'tommcdo/vim-exchange'
Plug 'mattn/emmet-vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'easymotion/vim-easymotion'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'phpvim/phpcd.vim'
Plug 'tobyS/pdv'
Plug 'dag/vim-fish'
Plug 'terryma/vim-smooth-scroll'
Plug 'christoomey/vim-system-copy'
Plug 'kovetskiy/sxhkd-vim'
Plug 'michaeljsmith/vim-indent-object'
Plug 'vimwiki/vimwiki'
Plug 'tpope/vim-surround'
call plug#end()
set nu
set rnu
set noerrorbells
set expandtab
set smartindent
set nowrap
set mouse=c
set ignorecase
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
nnoremap <SPACE> <Nop>
let mapleader=" "
noremap <Leader>C :!xclip -sel clipboard
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 10, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 10, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 10, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 10, 4)<CR>
noremap <silent> <Leader>e :%!p %<CR>:q!<CR>
let g:vimwiki_list = [{
        \ 'path': '~/vimwiki',
        \ 'template_path': '~/vimwiki/templates/',
        \ 'template_default': 'default',
        \ 'syntax': 'markdown',
        \ 'ext': '.md',
        \ 'path_html': '~/vimwiki/site_html/',
        \ 'custom_wiki2html': 'vimwiki_markdown',
        \ 'template_ext': '.tpl'}]

let g:multi_cursor_use_default_mapping=0

" Default mapping
let g:multi_cursor_start_word_key      = '<C-n>'
let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-s>'
let g:multi_cursor_quit_key            = '<Esc>'
