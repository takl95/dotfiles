fish_vi_key_bindings
set PROJECTS_DIR ~/projects
set EDITOR vim
set BROWSER google-chrome-stable
set -U FZF_ENABLE_OPEN_PREVIEW 1
set -e FZF_FIND_FILE_COMMAND 
set -U FZF_FIND_FILE_COMMAND "fd --hidden --no-ignore-vcs --ignore-file ~/.config/fd/.ignore --type f . \$dir"
abbr -a tst timew summary today
abbr -a tet timew export today
abbr -a tsi timew summary :ids
abbr -a cb2sn xclip -o -sel clipboard | sncli create -
abbr -a cd.. cd ..
abbr -a g git
abbr -a gpt git push --tags
abbr -a pt "commandline -r -- (pet search)"
abbr -a clone git clone
abbr -a cww vim ~/vimwiki/index.md
abbr -a cdw cd ~/vimwiki/
abbr -a kal ikhal
abbr -a scratchterm terminal -title scratchterm -e fish -c 'tmux new -s scratch' 
# docker
abbr -a ldo lazydocker
abbr -a dps docker ps
abbr -a dcu docker-compose up -d
abbr -a dcd docker-compose down --remove-orphans
abbr -a dcub docker-compose up -d --build
abbr -a dcb docker-compose build
abbr -a dex enter
function dexe
  docker exec -it (__enter_select_container) $argv
end
function edit_cmd --description 'Input command in external editor'
        set -l f (mktemp /tmp/fish.cmd.XXXXXXXX)
        if test -n "$f"
            set -l p (commandline -C)
            commandline -b > $f
            vim -c 'set ft=fish' $f
            commandline -r (more $f)
            commandline -C $p
            command rm $f
        end
    end

abbr -a p. p .
abbr -a gh. gh .

#Jira
function jr
        go run /home/ltakacs/go/src/github.com/mikepea/go-jira-ui/jira-ui/main.go $argv
end
function jrt
        jira list -q "project = AMS AND issuetype = Task AND text ~ \"netcup.de - Relaunch\" AND text ~ \"$argv*\"" | tac | tail -n 1 | awk -F ':' "{print \$1}" | xclip -selection clipboard
        jr (xclip -o -selection clipboard)
end
function relt
        jira login
        jira list -q "project = AMS AND issuetype = Task AND text ~ \"netcup.de - Relaunch\" AND text ~ \"$argv*\"" | tac | tail -n 1 | awk -F ':' "{print \$1}" | xclip -selection clipboard
        jira list -q "project = AMS AND issuetype = Task AND text ~ \"netcup.de - Relaunch\" AND text ~ \"$argv*\""
        echo -e '\n'
        echo (xclip -o -selection clipboard) yanked.

end
function find-ticket
        jira session > /dev/null
if test $status -eq 0
       set project (echo -e "AM\nAMS\nNCM\nCORPIT\nNCADM" | rofi -dmenu -i -p Project:)
       set query (zenity --entry --text "What are you looking for?")
       echo "(project = $project AND summary ~ \"$query*\" AND issuetype = Task AND status != Done ) OR key = \"$project-$query\" ORDER BY updated DESC, key DESC" | xclip -sel clipboard
       jira list -q "(project = $project AND summary ~ \"$query*\" AND issuetype = Task AND status != Done ) OR key = \"$project-$query\" ORDER BY updated DESC, key DESC" | rofi -dmenu -i -p Choose: | awk -F ':' "{print \$1}"
else
        terminal -e "jira login"
        zenity --info --text 'Login required. Run the command again afterwards' --width 200
       
end
end

#tasks
abbr -a vinbox vit -- -TAGGED
abbr -a tinvbox task -TAGGED
abbr -a vtp rtp
function rtp

        if count $argv > /dev/null
        vit (task projects | awk "NR>3" | awk 'NF' | fzf -m -q $argv | awk "{print \$1}" | tr '\n' ' ' | sed s/^/project:/)
else
        vit (task projects | awk "NR>3" | awk 'NF' | fzf -m | awk "{print \$1}" | tr '\n' ' ' | sed s/^/project:/)
end
end

abbr -a vt rt
function rt
        if count $argv > /dev/null
        vit (task tags | awk "NR>3" | awk 'NF' | sed '/^+/d'|  sed '/^next/d' | fzf -q $argv -m | awk "{print \$1}" | tr '\n' ' ' | sed s/^/+/)
else
        vit (task tags | awk "NR>3" | awk 'NF' | sed '/^+/d'|  sed '/^next/d' | fzf -m | awk "{print \$1}" | tr '\n' ' ' | sed s/^/+/)
end
end

#reminders
function remind
        python ~/.greminder/remind.py -i
end

# general
abbr -a t task
abbr -a nf notify-send 'Task done'
abbr -a spi sudo pacman -S
abbr -a spr sudo pacman -R
function kp --description "Kill processes"
          set -l __kp__pid (ps -ef | sed 1d | eval "fzf -q $argv $FZF_DEFAULT_OPTS -m --header='[kill:process]'" | awk '{print $2}')
            set -l __kp__kc $argv[1]

              if test "x$__kp__pid" != "x"
                          if test "x$argv[1]" != "x"
                                        echo $__kp__pid | xargs kill $argv[1]
                                            else
                                                          echo $__kp__pid | xargs kill -9
                                                              end
                                                                  kp
                                                                    end
                                                            end
# fun
abbr -a joke curl https://icanhazdadjoke.com
function weather
        curl wttr.in/Klagenfurt
end
# broot
abbr -a brs br -spd
function pp 
        ls $PROJECTS_DIR | rofi -dmenu -i -p Project: | xargs -r -I {} fish -c 'nohup phpstorm $PROJECTS_DIR/{} > /dev/null &'
end
function tp
        if count $argv > /dev/null
        set -U project (ls $PROJECTS_DIR | fzf -q $argv)
else
        set -U project (ls $PROJECTS_DIR | fzf )
end
echo $project
if not test -e ~/.tmuxp/$project.json
            cp ~/gscripts/tmuxp-template-ideapad.json ~/.tmuxp/$project.json
            sed -i 's/$PROJECT/'$project'/g' ~/.tmuxp/$project.json
end
tmuxp load -y $project
end
# editing config files
abbr -a ccf vim /home/ltakacs/.config/fish/config.fish
abbr -a cck vim /home/ltakacs/.config/sxhkd/sxhkdrc
abbr -a ccv vim /home/ltakacs/.vimrc
abbr -a cci vim /home/ltakacs/.i3/config
abbr -a cct vim /home/ltakacs/.tmux.conf
abbr -a ccw vim /home/ltakacs/.taskrc
abbr -a ccr vim ~/.config/ranger

# git
abbr -a gg lazygit
abbr -a gss git status -s
abbr -a gs git status

#yay

function yi
        if count $argv > /dev/null
        yay -S (yay -Slq | fzf -q $argv -m --preview 'yay -Si {1}' | sed 's/\n/ /g')
else
        yay -S (yay -Slq | fzf -m --preview 'yay -Si {1}' | sed 's/\n/ /g')
end
end

function yr

    if count $argv > /dev/null
        yay -R (yay -Qm | fzf -q $argv -m --preview 'yay -Si {1}' | sed 's/\n/ /g' | cut -d' ' -f1)
        k
else

        yay -R (yay -Qm | fzf -m --preview 'yay -Si {1}' | sed 's/\n/ /g' | cut -d' ' -f1)
end
end

function wtr
    if count $argv > /dev/null
        timew stop ; timew start "$argv"
    else
        timew tags | awk "NR>3" | awk 'NF' | sed '/^+/d' | tail -n +2 | awk -F ' -' "{print \$1}" | fzf | xargs -r -I '{}' timew start {}
    end
end
abbr wtrs timew stop
function tempod

        set results (timew tags | awk "NR>3" | awk 'NF' | sed '/^+/d' | tail -n +2 | awk -F ' -' "{print \$1}" | xargs -r -I '{}' -n1 timew summary today '{}')
if test $status -eq 0
        echo $results  
        echo ''
end
end
#screen recording
function rec2gif
   docker run --rm -v $PWD:/data asciinema/asciicast2gif -s 2 $argv
end
abbr -a rec asciinema rec
abbr -a recplay asciinema play

thefuck --alias | source
alias doh='commandline -i "sudo $history[1]";history delete --exact --case-sensitive doh'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

function p
        nohup phpstorm $argv > /dev/null & 
end
function pomo
        ~/gscripts/pomo $argv
end
function gh
        nohup github-desktop $argv > /dev/null &
end
function dk
	if string length (docker ps -aq)
		docker stop (docker ps -aq)
	end
end

function dr
	if string length (docker ps -aq)
		docker rm (docker ps -aq)
	end
end

function dkr
	dk && dr
end

function duden
 curl -s https://www.duden.de/suchen/dudenonline/$argv | pup section.vignette:first-of-type .vignette__label, section.vignette:first-of-type .vignette__snippet text{}  | sed '/^$/d' | sed 's/–.*//'
 end

function cdp
        if count $argv > /dev/null
        cd $PROJECTS_DIR/(ls $PROJECTS_DIR | fzf -q $argv)
else
        cd $PROJECTS_DIR/(ls $PROJECTS_DIR | fzf )
end
end

function cdpr
        if count $argv > /dev/null
        ranger $PROJECTS_DIR/(ls $PROJECTS_DIR | fzf -q $argv)
else
        ranger $PROJECTS_DIR/(ls $PROJECTS_DIR | fzf )
end
end

function fzf_complete
	set -l cmdline (commandline)
	# HACK: Color descriptions manually.
	complete -C | string replace -r \t'(.*)$' \t(set_color $fish_pager_color_description)'$1'(set_color normal) | fzf -d \t -1 -0 --ansi --header="$cmdline" --height="80%" --tabstop=4 | read -l token
	# Remove description
	set token (string replace -r \t'.*' '' -- $token)
	commandline -rt "$token"
end

bind \t 'fzf_complete; commandline -f repaint'
set -g fish_key_bindings fish_vi_key_bindings
bind -M insert \ck accept-autosuggestion
function ecb -d 'edit clipboard in vim'
        set EDITOR vim
        xclip -o -sel clip | vipe | xclip -selection c
end

function fs -d "Switch tmux session"
          tmux list-sessions -F "#{session_name}" | fzf | read -l result; and tmux switch-client -t "$result"
end
function __enter_container_id -d "Enter container with provided id"
  set selected_container $argv
  if docker exec "$selected_container" fish >/dev/null
    docker exec -it "$selected_container" fish
  else if docker exec "$selected_container" zsh >/dev/null
    docker exec -it "$selected_container" zsh
  else if docker exec "$selected_container" bash >/dev/null
    docker exec -it "$selected_container" bash
  else
    docker exec -it "$selected_container" sh
  end
end


function __enter_check_tools -d "Check if all necessary cli tools are installed"
  if not type -q docker
     echo "Docker not installed"
     return 1
  end

  if not type -q fzf
     echo "fzf not installed"
     return 1
  end
  return 0
end

function __enter_select_container -d "Select container via fzf"
  docker ps --filter status=running --format "table {{.Names}}\t{{.Image}}\t{{.ID}}" | awk 'NR > 1 { print }' | sort | read -z containers
  if [ -z "$containers" ];
      echo -e "No running container found"
      return 0
  end
  printf $containers | fzf -e --reverse | awk '{ print $3 }' | read selected_container; or return 1
  printf $selected_container
end


function enter -d "Interactively try to enter a docker container"
  __enter_check_tools; or return 1
  set selected_container (__enter_select_container); or return 1
  __enter_container_id $selected_container; or return 1
end
function record-todays-worklog
timew summary today
if read_confirm
        jira session > /dev/null
if test $status -eq 0
        timew export today | jq -c -M '.[] | {id: .id, start: .start, end: .end, ticket: .tags[0]}' | xargs -r -L 1 -I {} ~/gscripts/worklog-to-curl.js --data "{}" | xargs -r -L 1 -I {} fish -c '{}'
else
        terminal -e "jira login"
        zenity --info --text 'Login required. Run the command again afterwards' --width 200
       
end
end
end
function read_confirm --description 'Ask the user for confirmation' --argument prompt
    if test -z "$prompt"
        set prompt "Continue?"
    end 

    while true
        read -p 'set_color green; echo -n "$prompt [y/N]: "; set_color normal' -l confirm

        switch $confirm
            case Y y 
                return 0
            case '' N n 
                return 1
        end 
    end 
end
function fif
        rg --files-with-matches --no-messages "$argv" | fzf $FZF_PREVIEW_WINDOW --preview "rg --ignore-case --pretty --context 10 '$argv' {}"
end

set -gx PATH ~/bin ~/npm/bin ~/npm/ /home/ltakacs/.local/bin $PATH

